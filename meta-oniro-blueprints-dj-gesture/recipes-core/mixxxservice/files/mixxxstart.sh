#!/bin/sh
# SPDX-FileCopyrightText: 2021 Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

if test -z "$XDG_RUNTIME_DIR"; then
    export XDG_RUNTIME_DIR=/run/user/`id -u`
    if ! test -d "$XDG_RUNTIME_DIR"; then
        mkdir --parents $XDG_RUNTIME_DIR
        chmod 0700 $XDG_RUNTIME_DIR
    fi
fi

export DISPLAY=:0.0

# connect to the glove
/usr/bin/expect -f /usr/bin/gloveconnect.sh  D7:FB:AE:AE:D9:F8

sleep 5
# start mixxx
export XDG_SESSION_TYPE=x11
export WAYLAND_DISPLAY=wayland-1
export QT_QPA_PLATFORM=xcb
/usr/bin/mixxx --platform wayland-egl --settingsPath /run/mount/appdata/music -f &

