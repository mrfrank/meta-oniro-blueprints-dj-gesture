# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

require recipes-kernel/zephyr-kernel/zephyr-sample.inc

SUMMARY = "Gesture DJ Glove"
DESCRIPTION = "Zephyr based BLE MIDI Glove"
LICENSE = "Apache-2.0"

SRC_OPT_PROTO = "protocol=https"
SRC_OPT_DEST = "destsuffix=git/apps/gesture-glove"
SRC_OPT_NAME = "name=gesture-glove"
SRC_OPT_BRANCH = "branch=master"

SRC_OPTIONS = "${SRC_OPT_PROTO};${SRC_OPT_DEST};${SRC_OPT_NAME};${SRC_OPT_BRANCH}"
SRC_URI += "git://gitlab.eclipse.org/eclipse/oniro-blueprints/oniro-gesture-glove.git;${SRC_OPTIONS}"
SRC_URI[gesture-glove.sha256sum] = "372c4df49a257b5b9854cbc9272c0a752e5c87b53b7d662358ff61059cc05072"
SRCREV = "0541e878dd2bc6e7492cacf9a4c120f54a92045f"

ZEPHYR_SRC_DIR = "${S}/apps/gesture-glove"
